'use strict';

module.exports = require('greenlock-express');
module.exports._greenlockExpressCreate = module.exports.create;
module.exports.create = function (opts) {
  opts._communityPackage = opts._communityPackage || 'greenlock-koa';
  return module.exports._greenlockExpressCreate(opts);
};
