# Greenlock&trade; for Koa

An Automated HTTPS ACME client (Let's Encrypt v2) for Koa

Greenlock&trade; for
[Browsers](https://git.coolaj86.com/coolaj86/greenlock.html),
[Node.js](https://git.coolaj86.com/coolaj86/greenlock.js),
[Commandline](https://git.coolaj86.com/coolaj86/greenlock-cli.js),
[Express.js](https://git.coolaj86.com/coolaj86/greenlock-express.js),
[Node.js Cluster](https://git.coolaj86.com/coolaj86/greenlock-cluster.js),
[hapi](https://git.coolaj86.com/coolaj86/greenlock-hapi.js),
**Koa**,
and [rill](https://git.coolaj86.com/coolaj86/greenlock-rill.js)
| Sponsered by [ppl](https://ppl.family)

Features
========

  * [x] Automatic Registration via SNI (`httpsOptions.SNICallback`)
  * [x] Secure domain approval callback
  * [x] Automatic renewal between 10 and 14 days before expiration
  * [x] Virtual Hosting (vhost) with Multiple Domains & SAN
  * [x] and [more](https://git.coolaj86.com/coolaj86/greenlock-express.js)
  * [x] plugins for AWS, redis, and more

This module is just an alias for greenlock-express.js,
which works with any middleware system.

## Install

```
npm install --save greenlock-koa@2.x
```

QuickStart
==========

```javascript
'use strict';

//////////////////////
// Greenlock Setup  //
//////////////////////

var greenlock = require('greenlock-koa').create({
  version: 'draft-11' // Let's Encrypt v2
  // You MUST change this to 'https://acme-v02.api.letsencrypt.org/directory' in production
, server: 'https://acme-staging-v02.api.letsencrypt.org/directory'

, email: 'jon@example.com'
, agreeTos: true
, approveDomains: [ 'example.com' ]

  // Join the community to get notified of important updates
  // and help make greenlock better
, communityMember: true

, configDir: require('os').homedir() + '/acme/etc'

//, debug: true
});


//////////////////
// Just add Koa //
//////////////////

var http = require('http');
var https = require('https');
var koa = require('koa');
var app = new koa();

app.use(function *() {
  this.body = 'Hello World';
});

// https server
var server = https.createServer(greenlock.tlsOptions, greenlock.middleware(app.callback()));

server.listen(443, function () {
 console.log('Listening at https://localhost:' + this.address().port);
});


// http redirect to https
var http = require('http');
var redirectHttps = app.use(require('koa-sslify')()).callback();
http.createServer(greenlock.middleware(redirectHttps)).listen(80, function () {
  console.log('Listening on port 80 to handle ACME http-01 challenge and redirect to https');
});
```

Handling a dynamic list of domains
========================

If you handle multiple domains and you dynamically add new ones,
you'll want to replace the static list of domains in `approveDomains`
with a function like this:

```js
function approveDomains(opts, certs, cb) {
  // This is where you check your database and associated
  // email addresses with domains and agreements and such

  // The domains being approved for the first time are listed in opts.domains
  // Certs being renewed are listed in certs.altnames
  if (certs) {
    opts.domains = certs.altnames;
  }
  else {
    // Do something to
    opts.email = 'john.doe@example.com';
    opts.agreeTos = true;
  }

  opts.communityMember = true;

  // NOTE: you can also change other options such as `challengeType` and `challenge`
  // opts.challengeType = 'http-01';
  // opts.challenge = require('le-challenge-fs').create({});

  cb(null, { options: opts, certs: certs });
}
```

**SECURITY**: Be careful with this.
If you don't check that the domains being requested are the domains you
allow an attacker can make you hit your rate limit for failed verification
attempts.

See the
[vhost example](https://git.coolaj86.com/coolaj86/greenlock-express.js/src/branch/master/examples/vhost.js)
for an idea of how this is done.


More Usage & Troubleshooting
============================

See <https://git.coolaj86.com/coolaj86/greenlock-express.js>
